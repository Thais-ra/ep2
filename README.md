
# EP2 - OO 2019.2 (UnB - Gama)

- Turma Renato
- Data de entrega: 12/11/2019
- Matricula: 18/0078224

## Como jogar

1. Baixe o executavel localizado na pasta "execute"
2. Acesse a pasta com o arquivo baixado pelo terminal
3. No terminal digite:

```
java -jar JogoDaCobrinha.jar
```

## Descrição

Neste EP foi implementado o famoso jogo Snake (também conhecido como "jogo da cobrinha") é um jogo que ficou conhecido por diversas versões cuja versão inicial começou com o jogo Blockade de 1976, sendo feitas várias imitações em vídeo-games e computadores. No fim dos anos 90 foi popularizado em celulares da Nokia que vinham com o jogo já incluso.

O jogador controla uma longa e fina criatura que se arrasta pela tela, coletando comida (ou algum outro item), não podendo colidir com seu próprio corpo ou as "paredes" que cercam a área de jogo. Cada vez que a serpente come um pedaço de comida, seu rabo cresce, aumentando a dificuldade do jogo. O usuário controla a direção da cabeça da serpente (para cima, para baixo, esquerda e direita) e seu corpo segue.

## Comidas

O jogo possui três tipos de comida:
- Fruta: Ela aumenta o corpo da cobrinha em 1, e o jogador recebe mais 1 ponto. Ela se apresenta como uma fruta de cor laranja.
- Estrela: O corpo da cobrinha volta ao tamanho original, ela não fornece e nem retira pontos. Se apresenta como uma fruta de cor amarela.
- Doce: O corpo da cobrinha aumenta em 1 e o jogador recebe mais 2 pontos. Se apresenta como um doce rosa e azul.
- Bomba: A cobrinha morre e o jogador não recebe pontos por ela. Se apresenta como uma carinha branca.

## Tipos de Snakes

No jogo só foi implementada a cobrinha comum, que não possui nenhuma habilidade especial.

## Pontos

Os pontos são calculados de acordo com as frutas coletadas.
