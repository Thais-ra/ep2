package Jogo;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Campo extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	// Definições do Campo
    private final int LARGURA = 420;
    private final int ALTURA = 440;
    private final int TAMANHO_PONTO = 10;
    private final int TODOS_PONTOS = 1600;

	// Definição do plano cartesiano
    private int[] x = new int[TODOS_PONTOS];
    private int[] y = new int[TODOS_PONTOS];
    
    // Um valor aleatória para gerar posição
    private final int RAND_POSICAO = 40;
    
    // Um delay para o tempo de execução do jogo
    private final int DELAY = 100;

    // Pontos da cobrinha
    private int pontos;
    
    // Objetos
    private Fruta fruta = new Fruta();
    private Bomba bomba = new Bomba();
    private Comida objeto = new Estrela();
    
    // Pontuação
    private int PONTUAÇÃO = 0;
    String SCORE = "PONTUAÇÃO: " + PONTUAÇÃO;
    Font SCORE_FONT = new Font("Consolas", Font.BOLD, 12);
    FontMetrics SCORE_METRICA = this.getFontMetrics(SCORE_FONT);
    
	// Definição dos movimentos
    private boolean esquerda = false;
    private boolean direita = false;
    private boolean cima = false;
    private boolean baixo = false;

    // Denifição do status do jogo
    private boolean estaJogando = true;

    // Tempo de execução do jogo
    private Timer tempo;

    // Icones para formar a cobra
    private Image cabeça;
    private Image corpo;

    // Getters e Setters
    public int getPontos() {
    	return pontos;
    }
    
    public void setPontos(int pontos) {
    	this.pontos = pontos;
    }
    
    public int getPONTUAÇÃO() {
    	return PONTUAÇÃO;
    }
    
    public void setPONTUAÇÃO(int pONTUAÇÃO) {
    	PONTUAÇÃO = pONTUAÇÃO;
    }
    
    public void setEstaJogando(boolean estaJogando) {
    	this.estaJogando = estaJogando;
    }
    
    public Campo() {
    	// Cria uma instrução de teclado
        addKeyListener((KeyListener) new TAdapter());

        // Seta o plano de fundo
        setBackground(Color.BLACK);

        // Cria um icone do arquivo png e seta na imagem correspondente
        ImageIcon cabeça_ = new ImageIcon("../images/cabeça.png");
        cabeça = cabeça_.getImage();
        
        ImageIcon corpo_ = new ImageIcon("../images/corpo.png");
        corpo = corpo_.getImage();

        // Define o foco para o JPanel
        setFocusable(true);

        // Métodos
        iniciarJogo();	// Inicializa do jogo
    }

    public void iniciarJogo() {
        // Tamanho inicial da cobrinha
        pontos = 4;

        // Define a posição em (x,y) de cada ponto
        for (int i = 0; i < pontos; i++) {
            x[i] = 50 - i*10;
            y[i] = 50;
        }

        // Gera a primeira comida
        localFruta();

        // Inicia o tempo de execução do jogo
        tempo = new Timer(DELAY, this);
        tempo.start();
    }

    // Método para desenhar elementos na tela do jogo
    public void paint (Graphics g) {
        // Define o atribuito para a classe própria
        super.paint(g);
        // Desenha as comidas no plano (x,y) se o jogo estiver rodando
        if (estaJogando) {
        	g.drawImage(fruta.getIcone(), fruta.getX(), fruta.getY(), this);
        	
        	if (PONTUAÇÃO >= 2) {
        		g.drawImage(bomba.getIcone(), bomba.getX(), bomba.getY(), this);
        	}
        	
        	if (PONTUAÇÃO >= 15) {
        		g.drawImage(objeto.getIcone(), objeto.getX(), objeto.getY(), this);   
        		
        	} else if (pontos % 10 == 0) {
        		g.drawImage(objeto.getIcone(), objeto.getX(), objeto.getY(), this);
        	}
        	
            // Para cada ponto da cobrinha, desenha a cabeça e o corpo
            for (int i = 0; i < pontos; i++) {
                if (i == 0) {
                	g.drawImage(cabeça, x[i], y[i], this); 
                } 
                else {
                	g.drawImage(corpo, x[i], y[i], this); 
                }
            }

            // Desenha a pontuação na tela
            desenharPontuacao(g);

            // Executa a sincronia de dados
            Toolkit.getDefaultToolkit().sync();

            // Pausa os gráficos
            g.dispose();
        }
        else {
            FimDeJogo(g);
        }
    }

    public void desenharPontuacao (Graphics g) {
        SCORE = "PONTUAÇÃO: " + PONTUAÇÃO;
        SCORE_METRICA = this.getFontMetrics(SCORE_FONT);
        g.setColor(Color.white);
        g.setFont(SCORE_FONT);
        g.drawString(SCORE, (LARGURA - SCORE_METRICA.stringWidth(SCORE)) - 10, ALTURA - 10);
    }

    public void FimDeJogo (Graphics g) {
        String msg = "FIM DE JOGO! Sua pontuação: " + PONTUAÇÃO;
        Font pequena = new Font("Consolas", Font.BOLD, 14);
        FontMetrics metrica = this.getFontMetrics(pequena);
        g.setColor(Color.white);
        g.setFont(pequena);
        g.drawString(msg, (LARGURA - metrica.stringWidth(msg)) / 2, ALTURA / 2);
    }

    // Método para checar se a cobrinha comeu a comida
    public void checarComida() {
        if((x[0] == fruta.getX()) && (y[0] == fruta.getY())) {
           fruta.Consequencia();
            objeto.setX(600);
            objeto.setY(600);
            
            if(PONTUAÇÃO % 15 == 0)
            	localDoce();
            if(pontos % 5 == 0)
            	localBomba();
            if(pontos % 10 == 0)
            	localEstrela();
            localFruta();
        }
        
        else if ((x[0] == objeto.getX()) && (y[0] == objeto.getY())) { 
        	objeto.Consequencia();
            localFruta();
            objeto.setX(600);
            objeto.setY(600);
        }
        
        else if ((x[0] == bomba.getX()) && (y[0] == bomba.getY())) {
        	estaJogando = false;
        }
    }

    // Método para mover a cobrinha na tela
    public void mover() {
        // Para cada ponto da cobrinha desenha em (x,y)
        for (int i = pontos; i > 0; i--) {
            x[i] = x[(i - 1)];
            y[i] = y[(i - 1)];
        }

        // Se for para esquerda decrementa em x
        if (esquerda) {
            x[0] -= TAMANHO_PONTO;
        }

        // Se for para direita incrementa em x
        if (direita) {
            x[0] += TAMANHO_PONTO;
        }

        // Se for para cima decrementa em y
        if (cima) {
            y[0] -= TAMANHO_PONTO;
        }

        // Se for para baixo incrementa em y
        if (baixo) {
            y[0] += TAMANHO_PONTO;
        }
    }

    // Método para checar colisão entre a cobrinha e as bordas do jogo
    public void checarColisão() {
        // Para cada ponto, verifica se este está em posição com outro ponto
        // se estiver ele avista que o jogador parou de jogar devido a colisão
        for (int i = pontos; i > 0; i--) {
            if ((i > 4) && (x[0] == x[i]) && (y[0] == y[i]))
            { estaJogando = false; }
        }

        // Verifica se a cabeça da cobrinha encostou em algum ponto (x,y)
        // nas bordas (width,height) da tela
        if (y[0] > ALTURA)
        { estaJogando = false; }

        if (y[0] < 0)
        { estaJogando = false; }

        if (x[0] > LARGURA)
        { estaJogando = false; }

        if (x[0] < 0)
        { estaJogando = false; }
    }

    // Define um valor aleatório e atribui a uma posição x e y na tela para as comidas
    public void localFruta() {
        int random_fruta = (int) (Math.random() * RAND_POSICAO);
        fruta.setX(random_fruta * TAMANHO_PONTO);

        random_fruta = (int) (Math.random() * RAND_POSICAO);
        fruta.setY(random_fruta * TAMANHO_PONTO);
    }
    
   public void localEstrela() {
	   objeto = new Estrela();
	   int random_estrela = (int) (Math.random() * RAND_POSICAO);
	   objeto.setX(random_estrela * TAMANHO_PONTO);
    	
    	random_estrela = (int) (Math.random() * RAND_POSICAO);
    	objeto.setY(random_estrela * TAMANHO_PONTO);
    	
    }
   
   public void localDoce() {
	   objeto = new Doce();
	   int random_doce = (int) (Math.random() * RAND_POSICAO);
	   objeto.setX(random_doce * TAMANHO_PONTO);
   	
	   random_doce = (int) (Math.random() * RAND_POSICAO);
	   objeto.setY(random_doce * TAMANHO_PONTO);
   	
   }
    
    public void localBomba() {
 	   int random_bomba = (int) (Math.random() * RAND_POSICAO);
 	   bomba.setX(random_bomba * TAMANHO_PONTO);
    	
 	   random_bomba = (int) (Math.random() * RAND_POSICAO);
 	   bomba.setY(random_bomba * TAMANHO_PONTO);
    	
    }
    
    // Método de ações durante a execução do jogo
    public void actionPerformed (ActionEvent e) {
        if (estaJogando) {
            checarComida();
            checarColisão();
            mover();
        }
        repaint();
    }

    // Classe para analisar o teclado
    private class TAdapter extends KeyAdapter{

        // Método para verificar o que foi teclado
        @Override
        public void keyPressed(KeyEvent e) {
            // Obtém o código da tecla
            int key =  e.getKeyCode();

            // Verifica os movimentos e manipula as variáveis, para movimentar
            if ((key == KeyEvent.VK_LEFT) && (!direita)) {
                esquerda = true;
                cima = false;
                baixo = false;
            }

            if ((key == KeyEvent.VK_RIGHT) && (!esquerda)) {
                direita = true;
                cima = false;
                baixo = false;
            }

            if ((key == KeyEvent.VK_UP) && (!baixo)) {
                cima = true;
                esquerda = false;
                direita = false;
            }

            if ((key == KeyEvent.VK_DOWN) && (!cima)) {
                baixo = true;
                esquerda = false;
                direita = false;
            }
        }
    }
}
