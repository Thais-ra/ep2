package Jogo;

import javax.swing.ImageIcon;

public class Fruta extends Comida {
	public Fruta() {
		setX(0);
		setY(0);
		comida = new ImageIcon("../images/fruta.png");
		icone = comida.getImage();
	}
	
	public void Consequencia() {
		Main.cenario.setPontos(Main.cenario.getPontos() + 1);
        Main.cenario.setPONTUAÇÃO(Main.cenario.getPONTUAÇÃO() + 1);
	}
}
