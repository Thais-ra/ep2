package Jogo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Menu extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	
	public Menu() {
		setTitle("--- Jogo da Cobrinha ---");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 420, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnJogar = new JButton("Jogar");
		btnJogar.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Jogar();
			}
		});
		
		btnJogar.setBounds(154, 315, 114, 25);
		contentPane.add(btnJogar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		btnSair.setBounds(154, 352, 114, 25);
		contentPane.add(btnSair);
		
		JLabel textinho = new JLabel();
		textinho.setText("Selecione a opção desejada");
		textinho.setBounds(114, 276, 194, 21);
		contentPane.add(textinho);
		
		JLabel mostra_imagem = new JLabel("Imagem");
		mostra_imagem.setHorizontalAlignment(SwingConstants.CENTER);
		mostra_imagem.setIcon(new ImageIcon("../images/fundo.jpg"));
		mostra_imagem.setBounds(50, 43, 324, 195);
		contentPane.add(mostra_imagem);
	}
}
