package Jogo;

import javax.swing.ImageIcon;

public class Bomba extends Comida {
	public Bomba() {
		setX(0);
		setY(0);
		comida = new ImageIcon("./images/bomba.png");
		icone = comida.getImage();
	}
	public void Consequencia() {
		Main.cenario.setEstaJogando(false);
	}
}
