package Jogo;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Comida {
	private int x;
	private int y;
	protected Image icone;
	protected ImageIcon comida;
	
	
	private final int RAND_POSICAO = 40;
	private final int TAMANHO_PONTO = 10;
	
	public Image getIcone() {
		return icone;
	}
	
	public ImageIcon getComida() {
		return comida;
	}
	
	public int getRAND_POSICAO() {
		return RAND_POSICAO;
	}
	
	public int getTAMANHO_PONTO() {
		return TAMANHO_PONTO;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public Comida() {
		setX(0);
		setY(0);
		comida = new ImageIcon("../images/corpo.png");
		icone = comida.getImage();
	}
	
	public void Consequencia() {
		
	}
}
