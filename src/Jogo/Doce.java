package Jogo;

import javax.swing.ImageIcon;

public class Doce extends Comida {
	public Doce() {
		setX(0);
		setY(0);
		comida = new ImageIcon("../images/doce.png");
		icone = comida.getImage();
	}
	
	public void Consequencia() {
		Main.cenario.setPontos(Main.cenario.getPontos() + 2);
        Main.cenario.setPONTUAÇÃO(Main.cenario.getPONTUAÇÃO() + 2);
	}
}
