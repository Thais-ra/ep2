package Jogo;

import javax.swing.JFrame;

public class Jogar extends JFrame {
	private static final long serialVersionUID = 1L;

	public Jogar() {
		add(Main.cenario);

		 // Definições da janela
		 setTitle("--- Jogo da cobrinha ---");
		 setBounds(100, 100, 420, 440);
		 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 setResizable(false);
		 setVisible(true);
	}
}
