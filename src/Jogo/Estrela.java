package Jogo;

import javax.swing.ImageIcon;

public class Estrela extends Comida {
	public Estrela() {
		setX(0);
		setY(0);
		comida = new ImageIcon("../images/estrela.png");
		icone = comida.getImage();
	}
	
	public void Consequencia() {
		Main.cenario.setPontos(4);
	}
}
